import * as React from 'react';

//import { choferes } from '../../../choferes.json'

class Buscador extends React.Component {

    constructor() {
        super();
        this.state = {
            //choferes,
            nameSearch: '',
            switchValue: false,
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSwitch = this.handleSwitch.bind(this);
        //this.handleAddChofer = this.handleAddChofer.bind(this);
    }


    handleChange(e) {
        //console.log(e.target.value);
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSwitch(e) {
        this.setState({ switchValue: e.target.checked });
    }

    render() {
        const namesList = (this.props.choferesValues)
            .filter(chofer => {
                const cumpleFiltroNombre = chofer.nombre
                    .toLowerCase()
                    .indexOf(this.state.nameSearch.toLowerCase()) >= 0;
                const cumpleFiltroEstado = chofer.estado ===
                    (this.state.switchValue ? "Deshabilitado" : "Habilitado")
                return cumpleFiltroNombre && cumpleFiltroEstado;
            })
            .map(chofer => {
                return (
                    <>
                        <hr></hr>
                        <li key={chofer.id}>
                            {chofer.nombre}
                            <button className="btn btn-danger btn-sm ml-5" onClick={this.props.onRemoveChofer.bind(this, i)}>
                                Borrar
                            </button>
                        </li>
                    </>

                );
            });

        return (
            <div>

                <input
                    type="text"
                    name="nameSearch"
                    value={this.state.nameSearch}
                    onChange={this.handleChange}
                    className="form-control"
                    placeholder="Buscar chofer"
                    aria-label="Search"
                />

                <div class="custom-control custom-switch">
                    <input
                        class="custom-control-input"
                        type="checkbox"
                        //name="switchValue" 
                        onClick={this.handleSwitch}
                        id="customSwitches"
                    />
                    <label class="custom-control-label" for="customSwitches">Mostrar Deshabilitados</label>
                </div>

                <ul className="list-unstyled">
                    <hr />
                    {namesList}
                    <hr />
                </ul>

            </div>
        );
    }
}

export default Buscador;