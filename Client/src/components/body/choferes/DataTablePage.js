import * as React from 'react';
import { MDBDataTable } from 'mdbreact';
import { choferes } from '../../../choferes.json'

function DatatablePage () {
    
  const data = {

    columns: [
      {
        label: 'Nombre y Apellido',
        field: 'name',
        sort: 'asc',
        width: 270
      },
      {
        label: 'Taxi',
        field: 'taxi',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Estado',
        field: 'estado',
        sort: 'asc',
        width: 150
      },

    ],

    rows: [
      {
        nombre: "Gustavo Mamani",
        taxi:"imv920",
        provincia:"Jujuy",
        localidad:"San Salvador de Jujuy",
        estado:"Habilitado",
        usuario:"peinao18",
        pass:"peinao123"
      },
      {
        nombre: "Juan Perez",
        "taxi":"edu420",
        "provincia":"Jujuy",
        "localidad":"Perico",
        "estado":"Deshabilitado",
        "usuario":"juanp",
        "pass":"juan123"
      },
      {
        "nombre": "Franco Ruiz",
        "taxi":"kxi555",
        "provincia":"Jujuy",
        "localidad":"El Carmen",
        "estado":"Habilitado",
        "usuario":"franp",
        "pass":"franco123"
      },
      {
        "nombre": "Eduardo Castillo",
        "taxi":"ewq320",
        "provincia":"Jujuy",
        "localidad":"Reyes",
        "estado":"Deshabilitado",
        "usuario":"edup",
        "pass":"edu123"
      },
      {
        "nombre": "Carlos Cuevas",
        "taxi":"xcd320",
        "provincia":"Jujuy",
        "localidad":"Centro",
        "estado":"Habilitado",
        "usuario":"carlosc",
        "pass":"c123"
      },
      {
        "nombre": "Pablo Mendez",
        "taxi":"rmc319",
        "provincia":"Jujuy",
        "localidad":"San Martin",
        "estado":"Deshabilitado",
        "usuario":"pablom",
        "pass":"p456"
      },
      
    ]
  };

  return (
    <MDBDataTable
      striped
      bordered
      hover
      data={data}
    />
  );
}

export default DatatablePage;