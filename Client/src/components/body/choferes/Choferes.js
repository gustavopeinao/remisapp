import * as React from 'react';

import { choferes } from '../../../choferes.json'

import FormChofer from '../choferes/FormChofer'
import DatatablePage from './DataTablePage'
import Buscador from './Buscador'

class Choferes extends React.Component {

  constructor() {
    super();
    this.state = {
      choferes,
      nameSearch: '',
      switchValue: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSwitch = this.handleSwitch.bind(this);
    this.handleAddChofer = this.handleAddChofer.bind(this);
    // this.handleRemoveChofer = this.handleRemoveChofer.bind(this);
  }

  handleRemoveChofer(index){
    if (window.confirm("Esta seguro que quiere eliminar este Chofer ?")){
      this.setState({
        
        choferes: (this.state.choferes)
          .filter((chofer, i)=>{
            
            
            return (i !== index)
          })

      })
    }
  }

//   .filter(chofer => {
//     const cumpleFiltroNombre = chofer.nombre
//         .toLowerCase()
//         .indexOf(this.state.nameSearch.toLowerCase()) >= 0;
//     const cumpleFiltroEstado = chofer.estado ===
//         (this.state.switchValue ? "Deshabilitado" : "Habilitado")
//     return cumpleFiltroNombre && cumpleFiltroEstado;
// })

  handleAddChofer(chofer) {
    // var miestado = (chofer.estado == true)? "true":"false";
    // console.log(miestado);
    this.setState({
      choferes: [...this.state.choferes, chofer] //une el estado actual de choferes con el evento (objeto) que llega "chofer"
    });
  }

  handleChange(e) {
    //console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSwitch(e) {
    this.setState({ switchValue: e.target.checked });
  }

  render() {
  
    return (
      <div className="row">

        {/* ListaDeChoferes */}
        <div className="col-md-5">
          <h2>Lista de choferes</h2>
          <Buscador choferesValues={this.state.choferes} onRemoveChofer={this.handleRemoveChofer.bind(this, i)}/>
        </div>

        {/* AgregarNuevoChofer */}
        <div className="col-md-5 offset-md-1">
          <FormChofer onAddChofer={this.handleAddChofer} />
        </div>

      </div>
    );
  }
}

export default Choferes;