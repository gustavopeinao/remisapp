import * as React from 'react';


class FormChofer extends React.Component {

    constructor(){
        super()
        this.state = {
            id:'',
            nombre:'',
            taxi:'',
            provincia:'',
            localidad:'',
            estado:'',
            usuario:'',
            pass:'',  
        };
        //solo se bindea los metodos que modifican states del componente
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    } 

    handleChange(e){
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    handleClick(e){
        console.log('Boton ha sido clickeado');
    };

    handleSubmit(e){
        e.preventDefault();
        this.props.onAddChofer(this.state);
        //alert('enviando...')
        
    };


  render() {
    return (
       <div> 
           <h2>Agregar Nuevo Chofer</h2>
           <form className="card-body" onSubmit={this.handleSubmit}>

                <div className="form-group">
                    <label>Id</label>
                    <input
                        className="form-control"
                        type="text"
                        name="id"
                        // value={this.state.id}
                        onChange={this.handleChange}
                    />
                </div>

                <div className="form-group">
                    <label>Nombre y Apellido</label>
                    <input
                        className="form-control"
                        type="text"
                        name="nombre"
                        // value={this.state.nameAp}
                        onChange={this.handleChange}
                    />
                </div>

                <div className="form-group">
                    <label>Taxi</label>
                    <input
                        className="form-control"
                        type="text"
                        name="taxi"
                        // value={this.state.taxi}
                        onChange={this.handleChange}
                    />
                </div>

                <div className="form-group">
                    <label>Provincia</label>
                    <input
                        className="form-control"
                        type="text"
                        name="provincia"
                        // value={this.state.provincia}
                        onChange={this.handleChange}
                    />
                </div>

                <div className="form-group">
                    <label>Localidad</label>
                    <input
                        className="form-control"
                        type="text"
                        name="localidad"
                        // value={this.state.localidad}
                        onChange={this.handleChange}
                    />
                </div>

                <div className="form-group">
                    <label>Estado</label>
                    <select class="form-control" name="estado" onChange={this.handleChange}>
                        <option class="font-weight-light">Seleccionar estado </option>
                        <option>Habilitado</option>
                        <option>Deshabilitado</option>
                    </select>
                </div>

                <div className="form-group">
                    <label>Usuario</label>
                    <input
                        className="form-control"
                        type="text"
                        name="usuario"
                        // value={this.state.usuario}
                        onChange={this.handleChange}
                    />
                </div>

                <div className="form-group">
                    <label>Pass</label>
                    <input
                        className="form-control"
                        type="text"
                        name="pass"
                        // value={this.state.pass}
                        onChange={this.handleChange}
                    />
                </div>

                <button onClick={this.handleClick} className="btn btn-primary">
                    Guardar
                </button>
           </form>

       </div>
    );
  }


}

export default FormChofer;