import * as React from 'react';
import { HashRouter, Switch, Route } from "react-router-dom";

import { choferes } from '../../../choferes.json'

import Choferes from '../choferes/Choferes';

class Home extends React.Component {

  constructor() {
    super();
      this.state = {
          choferes,
      }
  }

   
  render() {
    return (
       <div class="row">
            <div class="col-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a 
                        class="nav-link active" 
                        id="v-pills-choferes-tab" 
                        data-toggle="pill" 
                        href="#v-pills-choferes" 
                        role="tab" 
                        aria-controls="v-pills-choferes" 
                        aria-selected="true"
                    >
                        Choferes
                        <span className="badge badge-pill badge-light ml-2">
                        { this.state.choferes.length }
                        </span>
                    </a>

                    <a 
                        class="nav-link" 
                        id="v-pills-taxis-tab" 
                        data-toggle="pill" 
                        href="#v-pills-taxis" 
                        role="tab" 
                        aria-controls="v-pills-taxis" 
                        aria-selected="false"
                    >
                        Taxis
                    </a>
                </div>
            </div>

            <div class="col-10">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" 
                         id="v-pills-choferes" 
                         role="tabpanel" 
                         aria-labelledby="v-pills-choferes-tab">
                        <Choferes />
                    </div>
                    <div class="tab-pane fade" 
                         id="v-pills-taxis"
                         role="tabpanel" 
                         aria-labelledby="v-pills-taxis-tab">
                    Panel de Taxis
                    </div>
                </div>
            </div>
            
        </div>
    );
  }
}

export default Home;