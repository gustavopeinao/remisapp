'use strict'
const usuarioModel = require('./usuarioModel')

function getUsuario(req, res) {
    let usuarioId = req.params.usuarioId

    usuarioModel.findById(usuarioId, (err, usuario) => {
        if (err) return res.status(500).send(message=`Error al realizar la peticion de usuario: ${err}`)
        if (!usuario) return res.status(404).send(message= 'El ususario no existe')

        res.status(200).send({usuario})
    })
}

function getUsuarios(req, res) {
    usuarioModel.find({}, (err, usuarios) => {
        if (err) return res.status(500).send(message=`Error al realizar la peticion de usuario: ${err}`)
        if (!usuarios) return res.status(404).send(message= 'No existen usuarios')

        res.send(200, {usuarios})
    })
}

function saveUsuario(req, res) {
    console.log('POST /usuarios')
    console.log(req.body)

    let usuario = new usuarioModel()
    usuario.username = req.body.username
    usuario.password = req.body.password
    usuario.nombre = req.body.nombre
    usuario.apellido = req.body.apellido
    
    usuario.save((err, usuarioStored) => {
        if(err) res.status(500).send(`Error al guardar en la base de datos: ${err}`)

        res.status(200).send(usuario = usuarioStored)

    })
}

function updateUsuario(req, res) {
    let usuarioId = req.params.usuarioId
    let update = req.body

    usuarioModel.findByIdAndUpdate(usuarioId, update, {new: true}, (err, usuarioUpdated) => {
        if (err) res.status(500).send(message = `Error al actualizar el usuario: ${err}`)
        //si todo va bien devuelve un "usuario"(generico definido en ese momento) con los datos de usuarioUpdated
        res.status(200).send({usuarioUpdated})
        
    })
}

function deleteUsuario(req, res) {
    let usuarioId = req.params.usuarioId

    usuarioModel.findById(usuarioId, (err, usuario) => {
        if (err) res.status(500).send(message = `Error al borrar el usuario: ${err}`)

        usuario.remove(err => {
            if (err) res.status(500).send(message=`Error al eliminar el usuario: ${err}`)
            res.status(200).send(message='El usuario ha sido eliminado con exito')
        })
    })
}

module.exports = {
    getUsuario,
    getUsuarios,
    saveUsuario,
    updateUsuario,
    deleteUsuario
}