'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema

var UsuarioSchema = Schema({
    username: String,
    password: String,
    nombre: String,
    apellido: String,
    // mail: String,
    // direccion: String,
    // fechaNacimiento: Date,
    // valoracion: Number,
    // seguidores: [{type:  mongoose.Schema.ObjectId, ref:  'Usuario'}],
    // seguidos: [{type:  mongoose.Schema.ObjectId, ref:  'Usuario'}]
});

module.exports = mongoose.model('Usuario', UsuarioSchema)