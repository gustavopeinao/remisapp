var express = require('express');
var router = express.Router();
const secret = 'algo secreto';
const passport = require('passport');
const jwt = require('jsonwebtoken');
var usuarioModel = require('../usuario/usuarioModel');

router.post('/login', function(req, res){
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: info ? info.message : 'Login failed',
                user   : user
            });
        }

        req.login(user, {session: false}, (err) => {
            if (err) {
                res.send(err);
            }

            const token = jwt.sign(user.toJSON(), secret, {
                expiresIn: 604800 // 1 week
              });

            return res.json({user, token});
        });
    })
    (req, res);
})

router.post('/register', function(req, res){
    var usuario = new usuarioModel(req.body);
    usuario.save(function(err, respuesta) {
        if (err) {
            res.status(404).send(err)
        }
        res.send(respuesta)
    })
})

module.exports = router;