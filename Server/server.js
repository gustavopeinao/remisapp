
var mongoose = require('mongoose');
const app = require('./app')

// mongoose.connect('mongodb://localhost:27017/peinaoDB', { useNewUrlParser: true });

// app.listen(3001,'localhost', function(){
//     console.log('Servidor express en puerto 3001 y localhost peinao');
// })  


mongoose.connect('mongodb://localhost:27017/peinaoBD', { useNewUrlParser: true }, (err, res) => {
    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    console.log('Conexión a la Base de Datos establecida...')

    app.listen(3001,() => {
        console.log('API REST corriendo en puerto 3001 y localhost peinaoxxx');
    })  
})

