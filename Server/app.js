'use strict'

var express = require('express');
var bodyParser = require('body-parser'); 
var app = express();
//const usuarioModel = require('./components/usuario/usuarioModel')
const usuarioCtrl = require('./components/usuario/usuarioController')
//MIDLEWARES
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//configurar las peticiones entrantes, que tipo puede permitir
app.use((req, res,  next) =>  {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers',  'Authorization, X-API-KEY,  Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods',  'GET, POST, OPTIONS,  PUT,  DELETE');
    res.header('Allow', 'GET, POST, OPTIONS,  PUT,  DELETE');
  
    next();
  });

//PETICIONES

app.post('/usuario', usuarioCtrl.saveUsuario)
app.get('/usuario/:usuarioId', usuarioCtrl.getUsuario)
app.get('/usuario', usuarioCtrl.getUsuarios)
app.delete('/usuario/:usuarioId', usuarioCtrl.deleteUsuario)
app.put('/usuario/:usuarioId', usuarioCtrl.updateUsuario)

module.exports = app